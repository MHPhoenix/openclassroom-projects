# Un site de réservation pour disquaire (Source : https://openclassrooms.com/fr/courses/4425076-decouvrez-le-framework-django/4630701-tirez-parti-de-ce-cours)

## Ce exercice est celui du cours *Découvrez le framework Django* (https://openclassrooms.com/fr/courses/4425076-decouvrez-le-framework-django) offert par Céline Martinet Sanchez sur Openclassrooms.

Plus précisément, la boutique aimerait gérer son stock dans une interface d'administration et offrir à sa clientèle un catalogue affichant les produits disponibles.

Le catalogue en ligne aura les fonctionnalités suivantes :

    * Affichage des derniers CD ajoutés en base dans la page d'accueil,

    * Visualisation d'un CD en cliquant sur sa pochette ou sur son titre,

    * Recherche par auteur,

    * Demande de réservation par un formulaire présent sur chaque page de CD.

L'interface d'administration sera privée et accessible par un seul utilisateur. Voici les fonctionnalités demandées :

    * Ajout de disques, modification et suppression.

    * Recherche d'un disque par titre d'album ou référence.

    * Liste des demandes de réservation.

    * Possibilité d'indiquer qu'un album est indisponible. Dans ce cas, il n'apparaît plus dans le catalogue public.

Le Bateau Pirate vous a même fait parvenir une maquette de leurs attentes (https://docs.google.com/document/d/1Oqsm4S68C5Q0DI3h7GL4sKhso2gek4TUeyGJesm3mn0/edit).

De quoi aurez-vous besoin pour réaliser ce projet ?

    * Une base de données : vous devrez conserver de nombreuses données ! Les albums, les artistes, les réservations... Afin de les gérer efficacement, vous aurez besoin d'une base de données.

    * Un service qui vous permet d'interagir avec la base de données de manière sécurisée.

    * Un serveur web dont le rôle va être de recevoir des requêtes HTTP, de les traiter et de renvoyer une réponse HTTP conforme.

    * Une interface d'administration pour gérer le contenu du site.

    * Bien d'autres éléments que nous verrons au fur et à mesure.

    Vous pourriez tout réaliser de zéro en pur Python. Néanmoins, vous vous doutez bien que cela serait extrêmement long et rébarbatif (sauf si vous adorez coder jour et nuit) ! Plutôt que de réinventer la roue, accueillez à bras ouverts Django !


* Environnement virtuel : DecouvrezDjango_OpenCR

* Pour creer une nouvelle base de donnees : createdb -O <user_name> <data_base_name>

* Sur linux (sudo -u postgres psql), pour lancer postgres

* Dans postgres, \l pour lister les databases

* \q, pour quitter postgres

* https://openclassrooms.com/fr/courses/4425076-decouvrez-le-framework-django/4630835-creez-un-nouveau-projet, pour les configurations